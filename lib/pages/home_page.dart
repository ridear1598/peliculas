import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomePage extends StatelessWidget {
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
         title: Text('Peliculas en cine'),
         backgroundColor: Colors.blueAccent,
         actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search), 
              onPressed: (){})
         ],
      ),
      body: Container(
          child: Column(
            children: <Widget>[
               _swiperTargetas()
            ],
          ),
        
      ),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.navigate_next_outlined),
      //   onPressed: (){
      //     print('hola');
      //   },
      // ),
    );
  }

 Widget _swiperTargetas() {
    return  Container(
      padding: EdgeInsets.only(top:10.0),
      width: double.infinity,
      height: 300.0,
      child: Swiper(
        itemBuilder: (BuildContext context,int index){
          return new Image.network("https://estaticos.muyinteresante.es/media/cache/1140x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-marron_0.jpg",fit: BoxFit.fill,);
        },
        itemCount: 3,
        itemWidth: 250.0,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
        layout: SwiperLayout.STACK,
      ),
      
    );
  }
}