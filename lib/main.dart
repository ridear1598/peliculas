
import 'package:flutter/material.dart';
import 'package:peluculas/pages/home_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Peliculas',
       //home: HomePage(),
      initialRoute: '/',
      routes:   {
        '/'   :(BuildContext context)=>HomePage(),
      },
    );
  }
}